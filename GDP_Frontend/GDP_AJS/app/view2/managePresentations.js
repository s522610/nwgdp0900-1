/**
 * Created by S522626 on 12/3/2015.
 */

var arrayList = [];
$(document).ready(function () {

    var trigger = $('.hamburger'),
        overlay = $('.overlay'),
        isClosed = false;

    trigger.removeClass('is-closed');
    trigger.addClass('is-open');
    $('#wrapper').toggleClass('toggled');

    trigger.click(function () {

        hamburger_cross();
    });

    function hamburger_cross() {

        if (isClosed == true) {
            //overlay.hide();
            trigger.removeClass('is-open');
            trigger.addClass('is-closed');
            isClosed = false;
        } else {
            //overlay.show();
            trigger.removeClass('is-closed');
            trigger.addClass('is-open');
            isClosed = true;
        }
    }

    $('[data-toggle="offcanvas"]').click(function () {

        $('#wrapper').toggleClass('toggled');
    });

});


angular.module('myApp', ['ngTouch', 'ui.grid', 'ui.grid.selection']).controller('tagsCtrl', ['$scope', '$http', '$log', '$timeout', 'uiGridConstants', function ($scope, $http, $log, $timeout, uiGridConstants){

    $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

    var selectedCourseID;
    var selectedSection;
    $scope.cName = '';
    $scope.id = '';
    $scope.course = [];
    $scope.sections = [];

    $(document.body).mask('loading ...');

    $http.get('../../../../GDP_Backend/API/getAllSections.php').success( function(response) {
        // console.log(response);
        var json_result = response;
        function1(json_result);
    });

    function function1(json_result){

        var courseObjArr = [];
        var couserObj = {};

        for(var i in json_result.section){
            var info = {
                id: json_result.section[i].CourseID,
                cName: json_result.section[i].CourseName
            };
            couserObj[json_result.section[i].CourseID] = json_result.section[i].CourseName;
        }
        for (value in couserObj){
            var tempCourse = {};
            tempCourse["id"] = value;
            tempCourse["cName"] = couserObj[value];
            $scope.course.push(tempCourse);
            tempCourse ={};
        }
        $scope.$apply;
        $(document.body).unmask();
    }


    $scope.updateBasedOnPresentationType = function(){


    };


    $scope.returnSectionNumber= function(section){



        return "section "+section.split("_")[1];
    };

    $scope.searchsections = function() {
        $(document.body).mask('loading ...');
        $scope.sections= [];
        $scope.assignments = [];
        var cid = $("#courseid").val();
        var courseid = {
            CourseID: cid
        };
        Object.toparams = function ObjecttoParams(obj) {
            var p = [];
            for (var key in obj) {
                p.push(key + '=' + encodeURIComponent(obj[key]));
            }
            return p.join('&');
        };
        var sectionObj = {};
        $http({
            url: '../../../../GDP_Backend/API/getSections.php',
            method: "POST",
            data: Object.toparams(courseid),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        })
            .then(function(response) {
                    selectedCourseID = $scope.courseid;
                    if (response.data.success == 1) {
                        // var count = parseInt(response.data.section);
                        var count =  response.data.section.length;
                        for(var i=0; i<count; i++) {
                            sectionObj= {
                                section: 'section_'+response.data.section[i].SectionNumber
                            };
                            $scope.sections.push(sectionObj);
                        }
                    }


                    // to load assignments
                    if (response.status == 200) {
                        var myarr = $scope.sections;
                        var myarr2 = [];
                        var assignmentObj = {};
                        var count = JSON.stringify($scope.sections.length);
                        for(var i=0;i<count;i++){
                            myarr2.push(myarr[i].section.split("_")[1])
                        }

                        var ourObj = {};
                        var cid = $("#courseid").val();
                        var info = {
                            'Course' : cid,
                            'Section' : myarr2
                        };
                        $http({
                            url: '../../../../GDP_Backend/API/assignment.php',
                            method: "POST",
                            data: Object.toparams(info),
                            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                        })
                            .then(function(response) {
                                    console.log(response);
                                    if (response.data.success == 1) {
                                        // var count = parseInt(response.data.section);
                                        var count =  response.data.assignment.length;



                                        for(var i=0; i<count; i++) {
                                            assignmentObj= {
                                                sid: response.data.assignment[i].CourseID,
                                                type: response.data.assignment[i].AssignmentType,
                                                fname: response.data.assignment[i].TopicName,
                                                ptime: response.data.assignment[i].PresentationTime,
                                                tname: response.data.assignment[i].SectionNumber,

                                            };
                                            $scope.assignments.push(assignmentObj);
                                        }
                                    }else {


                                        document.getElementById("messageArea").innerHTML ="There are no assignments under selected course and section";
                                        document.getElementById("titleArea").innerHTML ="Info";

                                        $("#classCreatedModal").modal('show');

                                        //  alert('There are no assignments under selected course and section'); //TODO change to new UI

                                    }
                                    $(document.body).unmask();
                                },
                                function(response) { // optional
                                    // failed
                                });
                    }

                    $scope.$apply;

                    document.getElementById("list").style.display = 'block';
                    var ref_this = $('ul.tabs').find('li.active').data('id');
                },
                function(response) { // optional
                    // failed
                });
    };

    $scope.gridOptions = {
        enableRowSelection: true,
        enableSelectAll: true,
        selectionRowHeaderWidth: 35,
        rowHeight: 35,
        showGridFooter:true
    };


    $scope.loadAddPresentationForm= function (){
        var selectedSection = $("ul#navlist li.active").text().trim().split(' ')[1];

        $http.get('../../../../GDP_Backend/API/getStudentByIdAndSection.php?CourseID='+selectedCourseID+'&SectionNumber='+selectedSection)
            .success(function(data) {
                console.log(data);
                $scope.gridOptions.data = data.student;

            });

    };

    $scope.gridOptions.columnDefs = [
        { name: 'ID' },
        { name: 'FirstName',
            name:'LastName'}

    ];

    $scope.gridOptions.multiSelect = true;



    $scope.info = {};



    $scope.selectAll = function() {
        $scope.gridApi.selection.selectAllRows();
    };

    $scope.clearAll = function() {
        $scope.gridApi.selection.clearSelectedRows();
    };

    $scope.toggleRow1 = function() {
        $scope.gridApi.selection.toggleRowSelection($scope.gridOptions.data[0]);
    };

    $scope.toggleFullRowSelection = function() {
        $scope.gridOptions.enableFullRowSelection = !$scope.gridOptions.enableFullRowSelection;
        $scope.gridApi.core.notifyDataChange( uiGridConstants.dataChange.OPTIONS);
    };

    $scope.setSelectable = function() {
        $scope.gridApi.selection.clearSelectedRows();
        $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
        $scope.gridOptions.data[0].age = 31;
        $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.EDIT);
    };

    $scope.gridOptions.onRegisterApi = function(gridApi){
        //set gridApi on scope
        $scope.gridApi = gridApi;
        gridApi.selection.on.rowSelectionChanged($scope,function(row){

            if(row.isSelected){
                $scope.addAssignmentForm.studentTable.$invalid = false;
            }else{
                $scope.addAssignmentForm.studentTable.$invalid = true;
            }
            var msg = 'row selected ' + row.isSelected;
            $log.log(msg);
        });

        gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows){

            if(rows.length == 0){
                $scope.addAssignmentForm.studentTable.$invalid = true;
            }else{
                $scope.addAssignmentForm.studentTable.$invalid = false;
            }
            var msg = 'rows changed ' + rows.length;
            $log.log(msg);
        });
    };



    $scope.addPresentation = function() {
        $(document.body).mask('loading ...');

        var stuList = [];
        var list = {};
        var ourObj = {};

        var sec = $('#navlist .active').text();
        var courseID = $("#courseid").val();
        var selectedStudents;
        var selectionType = $scope.selection;
        var topicName = $scope.tname;
        var teamName =  $("#teamname").val();
        var presentationTime = $scope.ptime;
        var section = sec.trim().split(" ")[1];
        var studentid = $scope.sid;
        var student = "";
        var info;
        if (selectionType == 'Group') {

            selectedStudents = $scope.gridApi.selection.getSelectedRows();
            $.each(selectedStudents, function (index, value) {

                student = student + value.ID + ",";
            });
            student = student.substr(0, student.length - 1);
            info = {
                'CourseID': courseID,
                'AssignmentType': selectionType,
                'TopicName': topicName,
                'PresentationTime': presentationTime,
                'StudentList': student,
                'SectionNumber': section,
                'TeamName' : teamName
            }

        } else if(selectionType == 'Individual') {

            selectedStudents = $scope.sid;
            student = selectedStudents;
            info = {
                'CourseID': courseID,
                'AssignmentType': selectionType,
                'TopicName': topicName,
                'PresentationTime': presentationTime,
                'StudentList': student,
                'SectionNumber': section
            }
        }else{
            selectedStudents = "Class";
            student = selectedStudents;
            info = {
                'CourseID': courseID,
                'AssignmentType': selectionType,
                'TopicName': topicName,
                'PresentationTime': presentationTime,
                'StudentList': courseID,
                'SectionNumber': section
            }
        }


        var assignments = [];

        var f = document.getElementById('questionsList').files[0],
            r = new FileReader();
        r.onloadend = function(e){
            var csvval=e.target.result.split("\n");
            for(var j=0;j<csvval.length-1;j++){
                var csvvalue=csvval[j].split(",");

                var assignment = {
                    type:csvvalue[0],
                    question:csvvalue[1],
                    response:csvvalue[2]
                };
                assignments.push(assignment);
            }
        };
        r.readAsText(f);
        var questions = {
            assignments : assignments
        };

        Object.toparams = function ObjecttoParams(obj) {
            var p = [];
            for (var key in obj) {
                p.push(key + '=' + encodeURIComponent(obj[key]));
            }
            return p.join('&');
        };
        $http({
            url: '../../../../GDP_Backend/API/AddAssignment.php',
            method: "POST",
            data: Object.toparams(info),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        })
            .then(function (response) {

                    console.log(response);
                    if (response.data.success == 1) {
                        addFeedbackQuestions(response);

                        assignmentObj = {
                            sid: info.CourseID,
                            type: info.AssignmentType,
                            fname: info.TopicName,
                            ptime: info.PresentationTime,
                            tname: info.SectionNumber,

                        };
                        console.log(assignmentObj);
                        $scope.assignments.push(assignmentObj);
                        $('#Add').modal('hide');
                        document.getElementById("messageArea").innerHTML ="Successfully added presentation.";
                        document.getElementById("titleArea").innerHTML ="Info";

                        //   }
                    }else if(response.data.success == 0){
                        if(response.data.message == "Failed to Create New Assignment"){
                            //failed to create new assignment

                            document.getElementById("messageArea").innerHTML ="Failed to Create New Assignment.";
                            document.getElementById("titleArea").innerHTML ="Info";
                        }
                        if(response.data.message == "Presentation Already Exists"){
                            // presentation already exist
                            document.getElementById("messageArea").innerHTML ="Presentation Already Exists";
                            document.getElementById("titleArea").innerHTML ="Info";
                        }
                        $('#Add').modal('hide');


                    }
                    $("#classCreatedModal").modal('show');
                    $(document.body).unmask();
                },
                function (response) { // optional
                    // failed
                });


        function addFeedbackQuestions(response){

            questions.assignmentid  = response.data.assignmentID;
            Object.toparams = function ObjecttoParams(obj) {
                var p = [];
                for (var key in obj) {
                    p.push(key + '=' + encodeURIComponent(obj[key]));
                }
                return p.join('&');
            };
            $http({
                url: '../../../../GDP_Backend/API/addFeedbackQuestions.php',
                method: "POST",
                data:  JSON.stringify(questions),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            })
                .then(function (response) {
                        console.log(response);
                        if (response.data.success == 1) {

                            //successfully added feedback questions

                        }

                    },
                    function (response) { // optional
                        // failed
                    });
        }

    }


}]);
