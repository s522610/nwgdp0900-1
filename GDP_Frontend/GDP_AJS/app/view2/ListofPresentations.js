/**
 * Created by S522612 on 11/21/2015.
 */
angular.module('myApp', []).controller('userCtrl', function($scope) {
    $scope.sid = '';
    $scope.fname = '';
    $scope.type = '';
    $scope.tname = '';
    $scope.edit = [
        { sid:'Hege', fname:"Pege", type:"individual", tname:" abc", id:1 },
        { sid:'Kim',  fname:"Pim", type:"individual", tname:" abc", id:1 },
        { sid:'Sal',  fname:"Smith", type:"individual", tname:" abc", id:1 },
        { sid:'Jack', fname:"Jones", type:"individual", tname:" abc", id:1 },
        { sid:'John', fname:"Doe", type:"individual", tname:" abc", id:1 },
        { sid:'Peter',fname:"Pan", type:"individual", tname:" abc", id:1 }
    ];
    $scope.edit = true;
    $scope.error = false;
    $scope.incomplete = false;

    $scope.editUser = function(id) {
        if (id == 'new') {
            $scope.edit = true;
            $scope.incomplete = true;
            $scope.sid = '';
            $scope.fname = '';
            $scope.type='';
            $scope.tname='';

        } else {
            $scope.edit = false;
            $scope.sid = $scope.users[id-1].sid;
            $scope.fname = $scope.users[id-1].fname;
            $scope.type = $scope.users[id-1].type;
            $scope.tname = $scope.users[id-1].tname;
        }
    };

    $scope.$watch('sid',function() {$scope.test();});
    $scope.$watch('fname',function() {$scope.test();});
    $scope.$watch('type', function() {$scope.test();});
    $scope.$watch('tname', function() {$scope.test();});

    $scope.test = function() {

        $scope.incomplete = false;
        if ($scope.edit && (!$scope.sid.length ||
            !$scope.fname.length || !$scope.type.length || !$scope.tname.length )) {
            $scope.incomplete = true;
        }
    };

});