/**
 * Created by S522612 on 11/9/2015.
 */

var arrayList = [];
$(document).ready(function () {

    var trigger = $('.hamburger'),
        overlay = $('.overlay'),
        isClosed = false;

    trigger.removeClass('is-closed');
    trigger.addClass('is-open');
    $('#wrapper').toggleClass('toggled');

    trigger.click(function () {

        hamburger_cross();
    });

    function hamburger_cross() {

        if (isClosed == true) {
            //overlay.hide();
            trigger.removeClass('is-open');
            trigger.addClass('is-closed');
            isClosed = false;
        } else {
            //overlay.show();
            trigger.removeClass('is-closed');
            trigger.addClass('is-open');
            isClosed = true;
        }
    }

    $('[data-toggle="offcanvas"]').click(function () {

        $('#wrapper').toggleClass('toggled');
    });

});

angular.module('myApp', []).controller('userCtrl', function($scope,$http) {
    $scope.fName = '';
    $scope.cName = '';
    $scope.lName = '';
    $scope.sec = '';
    $scope.startTiming = '';
    $scope.endTiming = '';
    $scope.users=[];



    $http.get('../../../../GDP_Backend/API/getAllSections.php').success( function(response) {
        console.log(response);
        var json_result = response;
        function1(json_result);
        $scope.students = response;
    });

    function function1(json_result){
        for(var i in json_result.section){
            var info = {
                id:i,
                fName: json_result.section[i].CourseID,
                cName: json_result.section[i].CourseName,
                lName: json_result.section[i].facultyname,
                sec: json_result.section[i].SectionNumber,
                startTiming: json_result.section[i].starttime,
                endTiming: json_result.section[i].endtime
            }
            $scope.users.push(info);
        }
        $scope.$apply;
    }

    $scope.edit = true;
    $scope.error = false;
    $scope.incomplete = false;
    $scope.currentId='new';

    $scope.updateOrAdd = function(){
        if($scope.currentId=='new') {

            var stuList = []
            var list = {}
            var ourObj = {};

            var f = document.getElementById('file').files[0],
                r = new FileReader();
            r.onloadend = function(e){
                var data = e.target.result;
                var x = data.split("\n");
                $.each(x,function(index,value){
                    if(value!=""){
                        list["index"+index]=value;
                    }

                });
                 stuList.push(list);
                ourObj.file = stuList
                console.log("final "+JSON.stringify(stuList));

                ourObj.course = [ {'CourseID': $("#courseid").val(),
                    'FacultyName': $("#facultyName").val(),
                    'SectionNumber': $("#section").val(),
                    'CourseName' : $("#courseName").val(),
                    'StartTime': $("#timepicker1").val(),
                    'EndTime': $("#timepicker2").val()}]

                $.ajax({
                    url:'../../../../GDP_Backend/API/CreateSection.php',
                    type: 'POST',
                    data:{
                        "info" : JSON.stringify(ourObj)
                    },
                    success: function (output) {
                        var json_result = JSON.parse(output);
                        if(json_result.success == 1)
                        {
                          //  $route.reload();
                        }
                        if(json_result.message == "No user found")
                        {
                        }
                        if(json_result.message == "Required field is missing")
                        {
                        }

                        document.getElementById("messageArea").innerHTML ="Successfully created class.";
                        document.getElementById("titleArea").innerHTML ="Info";
                        $("#newclass").hide();
                        $("#classCreatedModal").modal('show');

                    }

                });
            }

            r.readAsBinaryString(f);
              var d = new FormData();
            jQuery.each(jQuery('#file')[0].files, function(i, file) {
                d.append('file-'+i, file);
            });
            $scope.users.push({id:$scope.users.length+1,fName:$scope.fName,cName:$scope.cName, lName:$scope.lName,sec:$scope.sec,startTiming:$scope.startTiming,endTiming: $scope.endTiming })

        }else {

            $scope.users[$scope.currentId - 1].fName = $scope.fName;
            $scope.users[$scope.currentId - 1].cName = $scope.cName;
            $scope.users[$scope.currentId - 1].lName = $scope.lName;
            $scope.users[$scope.currentId - 1].sec = $scope.sec;
            $scope.users[$scope.currentId - 1].startTiming = $scope.startTiming;
            $scope.users[$scope.currentId - 1].endTiming = $scope.endTiming;


        }
        $scope.reset();
        $scope.$apply;

    };
    $scope.cancelAddClass = function(){

        $("#newclass").hide();

    }
    $scope.editUser = function(id) {

        $("#newclass").show();

        if (id == 'new') {
            $scope.currentId = 'new';
            $scope.edit = true;
            $scope.incomplete = true;

            var sendInfo = {
                CourseID: $("#courseid").val(),
                FacultyName: $("#facultyName").val(),
                SectionNumber: $("#section").val(),
                CourseName : $("#courseName").val(),
                StartTime: $("#timepicker1").val(),
                EndTime: $("#timepicker2").val(),

            };
            $.ajax({
                url:'../../../../GDP_Backend/API/CreateSection.php',
                type: 'POST',
                data:sendInfo,
                success: function (output) {
                    var json_result = JSON.parse(output);
                    if(json_result.success == 1)
                    {
                    }
                    if(json_result.message == "No user found")
                    {
                    }
                    if(json_result.message == "Required field is missing")
                    {
                    }
                }

            });

        } else {
            $scope.currentId = id;
            $scope.edit = false;
            $scope.fName = $scope.users[id-1].fName;
            $scope.cName = $scope.users[id-1].cName;
            $scope.lName = $scope.users[id-1].lName;
            $scope.sec = $scope.users[id-1].sec;
            $scope.startTiming = $scope.users[id-1].startTiming;
            $scope.endTiming = $scope.users[id-1].endTiming;

        }


    };

    $scope.$watch('fName', function() {$scope.test();});
    $scope.$watch('lName', function() {$scope.test();});

    $scope.test = function() {

        $scope.incomplete = false;
        if ($scope.edit && (!$scope.fName.length ||
            !$scope.lName.length )) {
            $scope.incomplete = true;
        }
    };

    //Written for the student insertion

    $scope.add = function(){
        var f = document.getElementById('file').files[0],
            r = new FileReader();
        r.onloadend = function(e){
            var data = e.target.result;

        }
        r.readAsBinaryString(f);
        var data = new FormData();
        jQuery.each(jQuery('#file')[0].files, function(i, file) {
            data.append('file-'+i, file);
        });

        //added for inserting students from CSV file
        $.ajax({

            url:'../../../../GDP_Backend/API/insertStudentsFromCSV.php',
            type: 'POST',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            success: function (output) {
                var json_result = JSON.parse(output);
                if(json_result.success == 1)
                {
                }
                if(json_result.message == "No user found")
                {
                }
                if(json_result.message == "Required field is missing")
                {
                }
            }

        });
    }

});



function saveChanges(){


}